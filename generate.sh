#!/bin/bash

./bin/dstep \
    ~/storj/uplink-c/.build/uplink/uplink_definitions.h \
    ~/storj/uplink-c/.build/uplink/uplink.h \
    --output ~/storj/uplink-d/source/generated

sed --in-place '/^import .*/a import uplink_definitions;' ./source/generated/uplink.d
