# Uplink example for D programming language

[Uplink](https://pkg.go.dev/storj.io/uplink) is the library used to connect to [Storj Decentralized Cloud Storage (DCS)](https://www.storj.io).

This repository is a demonstration how to use Uplink in D code. This has been tested on Linux. In this example libuplink is statically linked.

The program code is at (source/app.d)[source/app.d].

## Tools

- A D compiler
- [Dub](https://dub.pm/)
- Common Unix build tools
- [DStep](https://github.com/jacob-carlborg/dstep) to convert Uplink-C headers to D (not required to build and run the example)

## Instructions

Build and run example program:

```
make run
```

## Bindings

This example shows that you don't need to wait for bindings in order to use Uplink-C in your D-powered project.

Though Uplink would be slightly easier to use in D if there were bindings to use idiomatic constructs.
