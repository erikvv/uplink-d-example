import std.stdio: write, writeln, stdin;
import uplink;
import uplink_definitions;
import std.conv: to;
import std.string;
import std.datetime.date;
import std.datetime.systime;

void main()
{
	writeln("Welcome to Storj");
	write("Enter your access grant: ");

	string accessGrant = stdin.readln().strip();

	auto parseAccessResult = uplink_parse_access(accessGrant.toStringz());
	scope(exit) parseAccessResult.uplink_free_access_result();

	if (parseAccessResult.error) {
		writeln(parseAccessResult.error.message.to!string);
		return;
	}

	auto projectResult = parseAccessResult.access.uplink_open_project();
	if (projectResult.error) {
		writeln(projectResult.error.message.to!string);
		return;
	}

	string option;
	do {
		writeln("Menu:");
		writeln("b) list buckets");
		writeln("o) list objects");
		writeln("u) upload object");
		writeln("d) download object");
		writeln("q) exit");

		option = stdin.readln().strip();

		switch (option) {
			case "b":
				printBuckets(projectResult.project);
				break;
			case "o":
				printObjects(projectResult.project);
				break;
			case "u":
				uploadObject(projectResult.project);
				break;
			case "d":
				downloadObject(projectResult.project);
				break;
			case "q":
				writeln("Bye");
				break;
			default:
				break;
		}
	} while (option != "q");
}

void printBuckets(UplinkProject* project) {
	auto it = project.uplink_list_buckets(null);
	scope(exit) it.uplink_free_bucket_iterator();

	while (it.uplink_bucket_iterator_next()) {
		auto error = it.uplink_bucket_iterator_err();
		if (error) {
			writeln(error.message.to!string);
			return;
		}

		auto bucket = it.uplink_bucket_iterator_item();
		auto sysTime = SysTime.fromUnixTime(bucket.created);

		writeln("- " ~ sysTime.toString() ~ " " ~ bucket.name.to!string);
	} 
}

void printObjects(UplinkProject* project) {
	write("Enter bucket: ");
	auto bucket = stdin.readln().strip();

	UplinkListObjectsOptions opts = {
		recursive: true,
		system: true,
	};
	auto it = project.uplink_list_objects(bucket.toStringz, &opts);
	scope(exit) it.uplink_free_object_iterator();

	while (it.uplink_object_iterator_next()) {
		auto error = it.uplink_object_iterator_err();
		if (error) {
			writeln(error.message.to!string);
			return;
		}

		auto object = it.uplink_object_iterator_item();
		auto sysTime = SysTime.fromUnixTime(object.system.created);
		
		writeln("- " ~ sysTime.toString() 
			~ " " ~ object.key.to!string 
			~ " (" ~ object.system.content_length.to!string ~ " B)");
	}
}

void uploadObject(UplinkProject* project) {
	write("Enter bucket: ");
	auto bucket = stdin.readln().strip();
	write("Enter object key: ");
	auto key = stdin.readln().strip();
	write("Enter object content: ");
	auto content = stdin.readln().strip();

	auto uploadResult = project.uplink_upload_object(bucket.toStringz(), key.toStringz(), null);
	scope(exit) uploadResult.uplink_free_upload_result();
	if (uploadResult.error) {
		writeln(uploadResult.error.message.to!string);
		return;
	}

	auto upload = uploadResult.upload;
	auto writeResult = upload.uplink_upload_write(cast(void*) content.ptr, content.length);
	scope(exit) writeResult.uplink_free_write_result();

	if (writeResult.error) {
		writeln(writeResult.error.message.to!string);
		auto error = upload.uplink_upload_abort();
		uplink_free_error(error);
		return;
	}

	auto error = upload.uplink_upload_commit();
	scope(exit) uplink_free_error(error);
	if (error) {
		writeln(error.message.to!string);
	}	
}


void downloadObject(UplinkProject* project) {
	write("Enter bucket: ");
	auto bucket = stdin.readln().strip();
	write("Enter object key: ");
	auto key = stdin.readln().strip();

	auto downloadResult = project.uplink_download_object(
		bucket.toStringz(), 
		key.toStringz(),
		null
	);
	scope(exit) downloadResult.uplink_free_download_result();

	if (downloadResult.error) {
		writeln(downloadResult.error.message.to!string);
		return;
	}

	auto download = downloadResult.download;

	immutable buffersize = 80;
	char[] content;
	while (true) {
		content.length += buffersize;
		auto readResult = download.uplink_download_read(&content[$-buffersize], buffersize);
		scope(exit) readResult.uplink_free_read_result();
		content.length -= buffersize - readResult.bytes_read;

		auto error = readResult.error;
		if (error) {
			if (error.code == -1) {
				// EOF
				break;
			}

			writeln(error.message.to!string);
			break;
		}	
	}
	writeln(content);
}

