
tmp/uplink-c:
	mkdir -p tmp
	git clone --branch v1.6.2 https://github.com/storj/uplink-c.git ./tmp/uplink-c

tmp/uplink-c/.build/uplink/uplink_definitions.h tmp/uplink-c/.build/uplink/uplink.h: tmp/uplink-c
	cd tmp/uplink-c; make build

source/generated/uplink_definitions.d: tmp/uplink-c/.build/uplink/uplink_definitions.h
	dstep tmp/uplink-c/.build/uplink/uplink_definitions.h \
		--output source/generated/uplink_definitions.d

source/generated/uplink.d: tmp/uplink-c/.build/uplink/uplink.h
	mkdir -p tmp	
	
	dstep tmp/uplink-c/.build/uplink/uplink.h \
	    --output tmp/uplink.d

	sed '/^import .*/a import uplink_definitions;' tmp/uplink.d > source/generated/uplink.d

uplink-d-example: source/generated/uplink.d source/generated/uplink_definitions.d
	dub build

.PHONY:
build: uplink-d-example

.PHONY:
run: source/generated/uplink.d source/generated/uplink_definitions.d
	dub run

.PHONY:
clean:
	rm -R tmp
