extern (C):

// Copyright (C) 2020 Storj Labs, Inc.
// See LICENSE for copying information.

alias uplink_const_char = const char;

struct UplinkHandle
{
    size_t _handle;
}

struct UplinkAccess
{
    size_t _handle;
}

struct UplinkProject
{
    size_t _handle;
}

struct UplinkDownload
{
    size_t _handle;
}

struct UplinkUpload
{
    size_t _handle;
}

struct UplinkEncryptionKey
{
    size_t _handle;
}

struct UplinkPartUpload
{
    size_t _handle;
}

struct UplinkConfig
{
    const(char)* user_agent;

    int dial_timeout_milliseconds;

    // temp_directory specifies where to save data during downloads to use less memory.
    const(char)* temp_directory;
}

struct UplinkBucket
{
    char* name;
    long created;
}

struct UplinkSystemMetadata
{
    long created;
    long expires;
    long content_length;
}

struct UplinkCustomMetadataEntry
{
    char* key;
    size_t key_length;

    char* value;
    size_t value_length;
}

struct UplinkCustomMetadata
{
    UplinkCustomMetadataEntry* entries;
    size_t count;
}

struct UplinkObject
{
    char* key;
    bool is_prefix;
    UplinkSystemMetadata system;
    UplinkCustomMetadata custom;
}

struct UplinkUploadOptions
{
    // When expires is 0 or negative, it means no expiration.
    long expires;
}

struct UplinkDownloadOptions
{
    // When offset is negative it will read the suffix of the blob.
    // Combining negative offset and positive length is not supported.
    long offset;
    // When length is negative, it will read until the end of the blob.
    long length;
}

struct UplinkListObjectsOptions
{
    const(char)* prefix;
    const(char)* cursor;
    bool recursive;

    bool system;
    bool custom;
}

struct UplinkListUploadsOptions
{
    const(char)* prefix;
    const(char)* cursor;
    bool recursive;

    bool system;
    bool custom;
}

struct UplinkListBucketsOptions
{
    const(char)* cursor;
}

struct UplinkObjectIterator
{
    size_t _handle;
}

struct UplinkBucketIterator
{
    size_t _handle;
}

struct UplinkUploadIterator
{
    size_t _handle;
}

struct UplinkPartIterator
{
    size_t _handle;
}

struct UplinkPermission
{
    bool allow_download;
    bool allow_upload;
    bool allow_list;
    bool allow_delete;

    // unix time in seconds when the permission becomes valid.
    // disabled when 0.
    long not_before;
    // unix time in seconds when the permission becomes invalid.
    // disabled when 0.
    long not_after;
}

struct UplinkPart
{
    uint part_number;
    size_t size; // plain size of a part.
    long modified;
    char* etag;
    size_t etag_length;
}

struct UplinkSharePrefix
{
    const(char)* bucket;
    // prefix is the prefix of the shared object keys.
    const(char)* prefix;
}

struct UplinkError
{
    int code;
    char* message;
}

enum UPLINK_ERROR_INTERNAL = 0x02;
enum UPLINK_ERROR_CANCELED = 0x03;
enum UPLINK_ERROR_INVALID_HANDLE = 0x04;
enum UPLINK_ERROR_TOO_MANY_REQUESTS = 0x05;
enum UPLINK_ERROR_BANDWIDTH_LIMIT_EXCEEDED = 0x06;
enum UPLINK_ERROR_STORAGE_LIMIT_EXCEEDED = 0x07;
enum UPLINK_ERROR_SEGMENTS_LIMIT_EXCEEDED = 0x08;

enum UPLINK_ERROR_BUCKET_NAME_INVALID = 0x10;
enum UPLINK_ERROR_BUCKET_ALREADY_EXISTS = 0x11;
enum UPLINK_ERROR_BUCKET_NOT_EMPTY = 0x12;
enum UPLINK_ERROR_BUCKET_NOT_FOUND = 0x13;

enum UPLINK_ERROR_OBJECT_KEY_INVALID = 0x20;
enum UPLINK_ERROR_OBJECT_NOT_FOUND = 0x21;
enum UPLINK_ERROR_UPLOAD_DONE = 0x22;

enum EDGE_ERROR_AUTH_DIAL_FAILED = 0x30;
enum EDGE_ERROR_REGISTER_ACCESS_FAILED = 0x31;

struct UplinkAccessResult
{
    UplinkAccess* access;
    UplinkError* error;
}

struct UplinkProjectResult
{
    UplinkProject* project;
    UplinkError* error;
}

struct UplinkBucketResult
{
    UplinkBucket* bucket;
    UplinkError* error;
}

struct UplinkObjectResult
{
    UplinkObject* object;
    UplinkError* error;
}

struct UplinkUploadResult
{
    UplinkUpload* upload;
    UplinkError* error;
}

struct UplinkPartUploadResult
{
    UplinkPartUpload* part_upload;
    UplinkError* error;
}

struct UplinkDownloadResult
{
    UplinkDownload* download;
    UplinkError* error;
}

struct UplinkWriteResult
{
    size_t bytes_written;
    UplinkError* error;
}

struct UplinkReadResult
{
    size_t bytes_read;
    UplinkError* error;
}

struct UplinkStringResult
{
    char* string;
    UplinkError* error;
}

struct UplinkEncryptionKeyResult
{
    UplinkEncryptionKey* encryption_key;
    UplinkError* error;
}

struct UplinkUploadInfo
{
    char* upload_id;

    char* key;
    bool is_prefix;
    UplinkSystemMetadata system;
    UplinkCustomMetadata custom;
}

struct UplinkUploadInfoResult
{
    UplinkUploadInfo* info;
    UplinkError* error;
}

struct UplinkCommitUploadOptions
{
    UplinkCustomMetadata custom_metadata;
}

struct UplinkCommitUploadResult
{
    UplinkObject* object;
    UplinkError* error;
}

struct UplinkPartResult
{
    UplinkPart* part;
    UplinkError* error;
}

struct UplinkListUploadPartsOptions
{
    uint cursor;
}

// Parameters when connecting to edge services
struct EdgeConfig
{
    // DRPC server e.g. auth.[eu|ap|us]1.storjshare.io:7777
    // Mandatory for now because this is no agreement on how to derive this
    const(char)* auth_service_address;

    // Root certificate(s) or chain(s) against which Uplink checks
    // the auth service.
    // In PEM format.
    // Intended to test against a self-hosted auth service
    // or to improve security.
    const(char)* certificate_pem;
}

struct EdgeRegisterAccessOptions
{
    // Wether objects can be read using only the access_key_id.
    bool is_public;
}

// Gateway credentials in S3 format
struct EdgeCredentials
{
    // Is also used in the linkshare url path
    const(char)* access_key_id;
    const(char)* secret_key;
    // Base HTTP(S) URL to the gateway.
    // The gateway and linkshare service are different endpoints.
    const(char)* endpoint;
}

struct EdgeCredentialsResult
{
    EdgeCredentials* credentials;
    UplinkError* error;
}

struct EdgeShareURLOptions
{
    // Serve the file directly rather than through a landing page.
    bool raw;
}

// we need to suppress 'pedantic' validation because struct is empty for now

struct UplinkMoveObjectOptions
{
}

struct UplinkUploadObjectMetadataOptions
{
}
